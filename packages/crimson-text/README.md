# Crimson Text

Crimson Text is the secondary font used by Extinction Rebellion

## Registry Scoping
As we're publishing to a GitLab registry, you'll need to create a `.npmrc` 
file in the root of your project containing the following

```
@xrmcr-digital:registry=https://gitlab.com/api/v4/packages/npm/
```

This will enable consumption of packages within the `@xrmcr-digital` namespace.

## Installation

`npm install --save @xrmcr-digital/fonts-crimson-text`

You can then import the font using the following syntax. 

```scss
@import '~@xrmcr-digital/fonts-crimson-text';
```

If you're not using WebPack with FileLoaders for font files, you can also
copy the font files and configure the following path before importing in your SCSS:

```scss
$xr-fonts-crimson-text-path: '~@xrmcr-digital/fonts-crimson-text/fonts';
``` 

It's been named `XR Crimson Text` as to avoid potential clashes with users who
have older versions of Crimson Text installed locally on their machines.

You can also reference it using CSS variables as `var(--font-crimson-text)`. 

# Licence

Crimson Text is provided by [Google Fonts](https://fonts.google.com/specimen/Crimson+Text?selection.family=Crimson+Text).
This is simply a wrapper around Crimson Text for easier consumption.

