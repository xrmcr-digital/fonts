# Fucxed Caps

Fucxed Caps is the primary font used by Extinction Rebellion.
This package has been created to facilitate the adoption of the font and
ease consistency when the font updates.


## Registry Scoping
As we're publishing to a GitLab registry, you'll need to create a `.npmrc` 
file in the root of your project containing the following

```
@xrmcr-digital:registry=https://gitlab.com/api/v4/packages/npm/
```

This will enable consumption of packages within the `@xrmcr-digital` namespace.

## Installation

`npm install --save @xrmcr-digital/fonts-fucxed-caps`

You can then import the font using the following syntax. 

```scss
@import '~@xrmcr-digital/fonts-fucxed-caps';
```


If you're not using WebPack with FileLoaders for font files, you can also
copy the font files and configure the following path before importing in your SCSS:

```scss
$xr-fonts-fucxed-caps-path: '~@xrmcr-digital/fonts-fucxed-caps/fonts';
``` 


It's been named `XR FUCXED CAPS` as to avoid potential clashes with users who
have older versions of FUCXED CAPS installed locally on their machines.

You can also reference it using CSS variables as `var(--font-fucxed-caps)`. 
